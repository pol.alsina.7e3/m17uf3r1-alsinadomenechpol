using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBanditAction : Actions
{
    GameObject player;
    public LayerMask layerMaskPlayer;
    float speed;
    public override void StartState()
    {
        ChangeLayerAnimation(1, 1);
        player = GameObject.Find("Player");
        speed = this.GetComponent<Enemies>().speed;
        agent.speed = speed;    
    }
    public override void FinishState()
    {
       // StartCoroutine(ChangeLayerAnimationWaiting);
        ChangeLayerAnimation(1, 0);
    }
    public void FollowPlayer()
    {
        agent.SetDestination(player.transform.position);
        animator.SetFloat("VelocityY", transform.position.y);
        animator.SetFloat("VelocityX", transform.position.x);
    }
}
