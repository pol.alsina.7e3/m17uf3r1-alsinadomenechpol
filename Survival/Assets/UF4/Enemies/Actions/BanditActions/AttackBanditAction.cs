using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBanditAction : Actions
{

    [SerializeField] private int damage = 10;
    public Collider[] collidersArms;
    GameObject player;
    bool attack;
    [SerializeField] float intervalAttackTime;

    protected override void Start()
    {
        base.Start();
        attack = false;
    }
    public override void StartState()
    {
        ChangeLayerAnimation(2,1);
        //ActivatesColliderArms();
        player = GameObject.Find("Player");
        attack = false;
    }
    public void UpdateState()
    {
        
        LookAtPlayer();
        if (!attack)
        {
            StartCoroutine(CanAttack());
            
        }
    }
    public override void FinishState()
    {
        ChangeLayerAnimation(2,0);
        //DesactivateColliderArms();
    }
    IEnumerator CanAttack()
    {
        attack = true;
        yield return new WaitForSeconds(intervalAttackTime);
        animator.SetTrigger("AttackNormal");
        ActivatesColliderArms();      
    }
    public void LookAtPlayer()
    {
        transform.LookAt(new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z));
    }
    public void ActivatesColliderArms()
    {
        Debug.Log("ACtivaT");
        for (int i = 0; i < collidersArms.Length; i++)
        {
            collidersArms[i].enabled = true;
        }
    }
    public void DesactivateColliderArms()
    {
        for (int i = 0; i < collidersArms.Length; i++)
        {
            collidersArms[i].enabled = false;
            Debug.Log("desactivat");
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        animator.ResetTrigger("AttackNormal");
        
        IDamageble damageble = other.gameObject.GetComponent<IDamageble>();

        if (attack && damageble != null)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                damageble.ApplyDamage(damage);
                DesactivateColliderArms();
                attack = false;
            }
        }
    }
}

