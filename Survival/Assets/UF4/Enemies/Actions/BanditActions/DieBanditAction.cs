using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieBanditAction : Actions
{
    public override void StartState()
    {
        Die();
    }
    public void Die()
    {
        animator.SetTrigger("IsDead");       
    }
    public void EndAnimationDie()
    {
        Destroy(this.gameObject);
    }
}
