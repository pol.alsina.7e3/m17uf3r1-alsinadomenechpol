using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolBanditAction : Actions
{
    public Transform[] patrolPoints;
    [SerializeField] private float patrolSpeed;
    int currentTarget;
    float changeTargetDistance = 0.8f;
    public float timeToWait;
    float time;

    public override void StartState()
    {
        ChangeLayerAnimation(0, 1);
        currentTarget = 0;
    }
    public override void FinishState()
    {
        ChangeLayerAnimation(0,0);
    }
    public void Patrol()
    {
        if (Vector3.Distance(this.transform.position, patrolPoints[currentTarget].position) <= changeTargetDistance)
        {
            GetNextTarget();
        }
        else
        {
            agent.destination = patrolPoints[currentTarget].position;
            animator.SetFloat("VelocityY", transform.position.y);
            animator.SetFloat("VelocityX", transform.position.x);
        }
    }
    void GetNextTarget()
    {
        currentTarget++;
        Debug.Log(currentTarget);
        if (currentTarget > patrolPoints.Length-1)
        {
            currentTarget = 0;
        }
    }

}
