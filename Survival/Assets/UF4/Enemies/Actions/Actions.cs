using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Actions : MonoBehaviour
{
    public Animator animator;
    protected NavMeshAgent agent;
    protected virtual void Start()
    {
        animator = this.GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = true;
    }
    public virtual void StartState() { }
    public virtual void FinishState() { }
    protected void ChangeLayerAnimation(int indexLayer, float activate)
    {
        animator.SetLayerWeight(indexLayer, activate);
    }
}
