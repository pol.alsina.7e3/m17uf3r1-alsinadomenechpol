using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemies : Characters, IDamageble
{
    //public HealthBar healthBar;
    public ScriptableState currentState;
    public ScriptableState die;

    public int damage { get; set; }

    //UnityEngine.AI.NavMeshAgent agent;

    protected override void Start()
    {
        base.Start();
    }
    protected virtual void Update()
    {
        currentState.action.OnUpdate(this);
    }
    public void StateTransition(ScriptableState state)
    {
        if (currentState.scriptableStateTransitions.Contains(state))
        {
            currentState.action.OnFinishedState(this);
            currentState = state;
            currentState.action.OnSetState(this);
        }
    }
    protected void CheckHealth()
    {
        if (currentHP <= 0)
        {
            StateTransition(die);
        }
    }

    public void ApplyDamage(int damageTaken)
    {
        currentHP -= damageTaken;
        CheckHealth();
    }
}
