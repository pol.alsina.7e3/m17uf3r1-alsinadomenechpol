using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class BanditBehaviour : Enemies
{
    public ScriptableState patrol, follow, attack;
    bool isOnAttackRange;
    public float rangeAlert;
    public float rangeAttack;
    public LayerMask layerMaskPlayer;
    public LayerMask layerMaskGround;
    bool beAlert = false;
    
    protected override void Start()
    {
        base.Start();
        currentState = patrol;
        currentHP = 1; 
    }

    protected override void Update()
    {
        base.Update();
        isOnAttackRange = Physics.CheckSphere(this.transform.position, rangeAttack, layerMaskPlayer);
        beAlert = Physics.CheckSphere(this.transform.position, rangeAlert, layerMaskPlayer);
        if (beAlert)
        {
            if (isOnAttackRange)
            {

                StateTransition(attack);
            }
            else
            {
                StateTransition(follow);
            }
            
        }
        else
        {
            StateTransition(patrol);
        }
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, rangeAlert);
        Gizmos.DrawWireSphere(transform.position, rangeAttack);
    }
    
}
