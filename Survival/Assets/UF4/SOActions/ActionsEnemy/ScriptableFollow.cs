using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableFollow", menuName = "ScriptableObjects2/ScriptableAction/ScriptableFollow")]

public class ScriptableFollow : ScriptablesActions
{

    public override void OnFinishedState(Enemies sc)
    {
        sc.GetComponent<FollowBanditAction>().FinishState();
    }

    public override void OnSetState(Enemies sc)
    {
        sc.GetComponent<FollowBanditAction>().StartState();
        
        
    }

    public override void OnUpdate(Enemies sc)
    {
        sc.GetComponent<FollowBanditAction>().FollowPlayer();
    }
}