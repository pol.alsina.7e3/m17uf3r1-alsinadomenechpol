using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ScriptableRun", menuName = "ScriptableObjects2/ScriptableAction/ScriptableRun")]

public class ScriptableRun : ScriptablesActions
{
    public override void OnFinishedState(Enemies sc)
    {
       
    }

    public override void OnSetState(Enemies sc)
    {
    
    }

    public override void OnUpdate(Enemies sc)
    {
      
    }

}
