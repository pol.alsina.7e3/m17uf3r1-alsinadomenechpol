using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptablesActions : ScriptableObject
{
    
    public abstract void OnFinishedState(Enemies sc);

    public abstract void OnSetState(Enemies sc);

    public abstract void OnUpdate(Enemies sc);

}
