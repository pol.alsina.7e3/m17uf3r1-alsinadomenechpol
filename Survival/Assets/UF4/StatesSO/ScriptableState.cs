using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableState", menuName = "ScriptableObjects2/ScriptableState")]
public class ScriptableState : ScriptableObject
{
    public ScriptablesActions action;
    public List<ScriptableState> scriptableStateTransitions;
}
