using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearCollider : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        IDamageble damageble = collision.gameObject.GetComponent<IDamageble>();
        if (damageble != null && collision.gameObject.CompareTag("Player"))
        {
            damageble.ApplyDamage(1);
        }

    }
}
  
