using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Characters : MonoBehaviour
{
    [SerializeField] protected int currentHP;
    public float speed;
    protected Animator animator;
    //protected Rigidbody rigidbody;
    protected Collider collider;
    private AudioSource audioSource;
    public Text infoCharacter; // name and level in case of enemies, npc only name
    //public DataCharacterSO dataCharacter;
    //HealthBar
    // so with data character
    protected virtual void  Start()
    {
        collider = GetComponent<Collider>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        //rigidbody = GetComponent<Rigidbody>();
        //get heath bar
    }
}
