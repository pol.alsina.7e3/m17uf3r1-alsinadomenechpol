using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimThirdPersonController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimCam;
    [SerializeField] private LayerMask layer;
    [SerializeField] private Transform aim;
    // Update is called once per frame
    void Update()
    {
        if (aimCam.gameObject)
        {
            Vector2 screenCenterPoint = new Vector2(Screen.width / 2, Screen.height / 2);
            Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
            if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, layer))
            {
                aim.position = raycastHit.point;
            }
        }
        
    }
}
