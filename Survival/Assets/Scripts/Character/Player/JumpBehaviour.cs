using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : MonoBehaviour
{
    public float jumpForce; 
    public float gravity;
    public float groundDistance;
    public LayerMask groundMask; 

    private CharacterController controller;
    private Vector3 velocity;
    private bool isGrounded;
    bool isJumping;
    Animator animator;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        isJumping = false;
    }

    void Update()
    {
        Jump();
    }
    void Jump()
    {
        isGrounded = Physics.CheckSphere(this.transform.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            isJumping = false;
            velocity.y = -2f;
        }

        if (InputManager.Instance.GetJumpButtonPressed() && isGrounded)
        {
            isJumping = true;
            velocity.y = Mathf.Sqrt(jumpForce * -1f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
        animator.SetBool("IsGrounded", isGrounded);
        animator.SetFloat("JumpVelocity", velocity.y);
        animator.SetBool("IsJumping", isJumping);
    }
}
