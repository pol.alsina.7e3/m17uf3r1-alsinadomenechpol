using Cinemachine;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.InputSystem.XR;

public class PlayerBehaviour : Characters, IDamageble
{
    [SerializeField] float _speedSprint;
    Vector3 direction;

    [Header("Jump")]
    [SerializeField] private float jumpForce;
    private CharacterController characterController;

    [Header("Cam")]
    public Transform cam;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    Vector2 movementInput;

    bool isCourch;
    bool canMove = true;

    [SerializeField] private CinemachineVirtualCamera aimCam;
    [SerializeField] private CinemachineVirtualCamera danceCam;

    public delegate void GameOver(bool hasWin);
    public static event GameOver gameOver;
    public int damage { get; set; }

    private void Awake()
    {
        characterController = this.GetComponent<CharacterController>();
        isCourch = false;
    }
    protected override void Start()
    {
        base.Start();

    }

    void Update()
    {
        if (canMove)
        {
            PlayerMovement();
            Animations();
            if (InputManager.Instance.GetCrouchButtonPressed()) isCourch = !isCourch;
            CheckIsAiming();
        }
        
        if (InputManager.Instance.inputs.GamePlay.Dance.WasPressedThisFrame())
        {
            canMove = false;
            animator.SetTrigger("Dance");
            danceCam.gameObject.SetActive(true);
        }
        if (InputManager.Instance.inputs.GamePlay.PickItem.WasPressedThisFrame())
        {
            canMove = false;
            animator.SetTrigger("TakeItem");
        }
    }

    void PlayerMovement()
    {
        float targetSpeed = InputManager.Instance.GetRunPressed() ? _speedSprint : speed;
        if (targetSpeed == _speedSprint) animator.SetBool("IsRunning", true); else animator.SetBool("IsRunning", false);
        if (InputManager.Instance.GetMovementPlayer() == Vector2.zero) targetSpeed = 0.0f;
        movementInput = InputManager.Instance.GetMovementPlayer();      
        direction = new Vector3(movementInput.x, 0, movementInput.y).normalized;
        //rigidbody.velocity =Vector3.up*rigidbody.velocity.y+ direction * targetSpeed * Time.deltaTime;
        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x,direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity,
               turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f)*Vector3.forward;
            characterController.Move(moveDir * targetSpeed * Time.deltaTime);
            //velocity = Vector3.up * GetComponent<Rigidbody>().velocity.y + moveDir * targetSpeed * Time.deltaTime;
        }
    }

   void Animations()
    {
        animator.SetFloat("VelX", movementInput.x);
        animator.SetFloat("VelY", movementInput.y);
        animator.SetBool("IsCrouch", isCourch);       
    }

    void CheckIsAiming()
    {
        if (InputManager.Instance.AimInput())
        {
            aimCam.gameObject.SetActive(true);
            animator.SetLayerWeight(1, 1f);
        }
        else
        {
            aimCam.gameObject.SetActive(false);
            animator.SetLayerWeight(1, 0f);
        }
    }
    public void EndDance()
    {
        canMove = true;
        animator.ResetTrigger("Dance");
        danceCam.gameObject.SetActive(false);
    }
    public void EndTakeItemAnimation()
    {
        canMove = true;
        animator.ResetTrigger("TakeItem");
    }

    public void ApplyDamage(int damageTaken)
    {
        currentHP -= damageTaken;
        CheckHp();
    }
    void CheckHp()
    {
        if (currentHP <= 0)
        {
            if (gameOver != null)
            {
                gameOver(false);
            }          
        }
    }
}
