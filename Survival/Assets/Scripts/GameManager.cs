using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int pickedItems;
    public AudioClip pickItemClip;
    public GameObject key;
    public AudioSource audioSource;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        audioSource = GetComponent<AudioSource>();
        pickedItems = 0;
    }
    public void PickedItem()
    {
        pickedItems++;
        audioSource.PlayOneShot(pickItemClip);
        CheckPickedItems();
    }
    void CheckPickedItems()
    {
        if (pickedItems == 3)
        {
            Instantiate(key, new Vector3(182.962f, 1.158f, 737.9174f), transform.rotation);
        }
    }
    
}

