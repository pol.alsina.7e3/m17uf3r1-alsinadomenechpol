using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;
    public MapInputPlayer inputs;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        inputs = new MapInputPlayer();
        inputs.GamePlay.Enable();
    }
    public Vector2 GetMovementPlayer()
    {
        return inputs.GamePlay.Move.ReadValue<Vector2>();
    }

    public bool GetRunPressed()
    {
        return inputs.GamePlay.Run.IsPressed();
    }
    public bool GetCrouchButtonPressed()
    {
        return inputs.GamePlay.Crouch.WasPressedThisFrame();
    }
    public bool GetJumpButtonPressed()
    {
        return inputs.GamePlay.Jump.WasPressedThisFrame();
    }
    public bool AimInput()
    {
        return inputs.GamePlay.Aiming.IsPressed();
    }
    public bool InteractionButton()
    {
        return inputs.GamePlay.PickItem.WasPressedThisFrame();
    }
    public bool ShootButton()
    {
        return inputs.GamePlay.Shoot.WasPressedThisFrame();
    }
}
