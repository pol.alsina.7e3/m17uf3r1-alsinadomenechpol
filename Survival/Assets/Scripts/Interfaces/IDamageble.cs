using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageble 
{
    public int damage { get; set; }
    void ApplyDamage(int damageTaken);
}
