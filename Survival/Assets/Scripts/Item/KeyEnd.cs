using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static KeyEnd;

public class KeyEnd : MonoBehaviour
{
    public delegate void DesactivatePannelItemsMissing();
    public static event DesactivatePannelItemsMissing desactivatePannelItemsMissing;
    private void OnCollisionEnter(Collision collision)
    {       
        if (collision.gameObject.CompareTag("Player"))
        {
            desactivatePannelItemsMissing();
            SceneManager.LoadScene("R1.2.2 Interiors");
        }
    }
}
