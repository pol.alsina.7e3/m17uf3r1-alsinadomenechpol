using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIHudManager : MonoBehaviour
{
    public GameObject pannelItemsToFind;
    public GameObject victory;
    public GameObject lose;
    public GameObject pannelGameOver;
    public Image[] iconItemsToFind;
    private void OnEnable()
    {
        PlayerBehaviour.gameOver += ActivateGameOverPannel;
        PickItem.gameOver += ActivateGameOverPannel;
        PickItem.itemPicked += CheckItemPicked;
        KeyEnd.desactivatePannelItemsMissing += DesactivatePannelItemsToFind;
    }

    private void OnDisable()
    {
        PickItem.gameOver -= ActivateGameOverPannel;
        PickItem.itemPicked -= CheckItemPicked;
        PlayerBehaviour.gameOver -= ActivateGameOverPannel;
        KeyEnd.desactivatePannelItemsMissing -= DesactivatePannelItemsToFind;
    }
    public void PlayAgain()
    {
        GameManager.instance.pickedItems = 0;      
        SceneManager.LoadScene("GameScene");
        pannelGameOver.SetActive(false);
        Time.timeScale = 1f;
    }
    void ActivateGameOverPannel(bool hasWin)
    {
        Time.timeScale = 0f;
        pannelGameOver.SetActive(true);
        victory.SetActive(hasWin);
        lose.SetActive(!hasWin);
        
    }
    void CheckItemPicked(Sprite icon)
    {
        Debug.Log("davant el for");
        for (int i = 0; i < iconItemsToFind.Length; i++)
        {
            Debug.Log("davant el if");
            if (icon.name == iconItemsToFind[i].sprite.name)
            {
                Debug.Log("passo el if");
                iconItemsToFind[i].gameObject.SetActive(false);
            }
        }
    }
    void DesactivatePannelItemsToFind()
    {
        pannelItemsToFind.SetActive(false);
    }
}
