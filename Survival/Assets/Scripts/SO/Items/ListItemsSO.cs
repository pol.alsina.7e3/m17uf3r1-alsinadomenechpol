using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListItemsSO", menuName = "ListItemsSO")]
public class ListItemsSO : ScriptableObject
{
    public List<ItemSO> items;  
    public List<GameObject> itemsGameObject;

    public void ClearLists()
    {
        items.Clear();
        itemsGameObject.Clear();
    }
}
