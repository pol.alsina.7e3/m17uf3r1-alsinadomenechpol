using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PickItem;
using static UnityEditor.Progress;

public class PickItem : MonoBehaviour
{

    public ListItemsSO Invetory;
    public GameObject mask;
    public GameObject backpack;
    public GameObject hatBody;
    public delegate void ItemPicked(Sprite icon);
    public static event ItemPicked itemPicked;
    public delegate void GameOver(bool hasWin);
    public static event GameOver gameOver;
    private void Awake()
    {
        Invetory.ClearLists();
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("Object"))
        {
            Destroy(other.gameObject.GetComponent<Animator>());
            Destroy(other);
            var itemSO = other.gameObject.GetComponent<ItemInfo>().ItemSO;
            GameManager.instance.PickedItem();
            Invetory.itemsGameObject.Add(other.gameObject);
            //Invetory.items.Add(other.gameObject.GetComponent<ItemInfo>().ItemSO);
            itemPicked(itemSO.iconItem);
            InstantiateCostume(itemSO, other.gameObject);
        }
        if (other.gameObject.CompareTag("End"))
        {         
            Debug.Log("end");
            gameOver(true);
        }

    }
    void InstantiateCostume(ItemSO itemPicked, GameObject gameObjectToMove)
    {
 
        
        if (itemPicked is MaskSO)
        {          
            StartCoroutine(WaitTimeMask(gameObjectToMove));
            gameObjectToMove.transform.position = Vector3.zero;
            
        }
        else if (itemPicked is BagSO)
        {
            StartCoroutine(WaitTimeBag(gameObjectToMove));
            gameObjectToMove.transform.position = Vector3.zero;
        }
        else if (itemPicked is HatSO)
        {
            StartCoroutine(WaitTimeHat(gameObjectToMove));
            //gameObjectToMove.transform.position = Vector3.zero;
        }
    }
    IEnumerator WaitTimeMask(GameObject gO)
    {
        //gO.transform.position = Vector3.zero;
        yield return new WaitForSeconds(0.01f);
        yield return new WaitForSeconds(0.001f);
        gO.transform.parent = mask.transform.parent;
        gO.transform.position = mask.transform.position;
        gO.transform.rotation = mask.transform.rotation;

    }
    IEnumerator WaitTimeBag(GameObject gO)
    {
        //gO.transform.position = Vector3.zero;
        yield return new WaitForSeconds(0.01f);
        yield return new WaitForSeconds(0.001f);
        gO.transform.parent = backpack.transform.parent;
        gO.transform.position = backpack.transform.position;
        gO.transform.rotation = backpack.transform.rotation;

    }
    IEnumerator WaitTimeHat(GameObject gO)
    {
        //gO.transform.position = Vector3.zero;
        yield return new WaitForSeconds(0.01f);
        yield return new WaitForSeconds(0.001f);
        gO.transform.parent = hatBody.transform.parent;
        gO.transform.position = hatBody.transform.position;
        //gO.transform.rotation = hatBody.transform.rotation;

    }
}
